//Repititon Control Structures

//While Loop
/* Syntax
	while (condition) {
		statements/s;
	}
*/

let count = 5;

// count = 0
while (count !== 0){
	console.log ("While: " + count);
	count--;
}

for(let i=0; i < 11; i++){
      if(i%2 === 0){
          console.log(i);
      }
  }

console.log("Displays number 1-10")
count = 1;

while (count < 11){
	console.log ("While: " + count);
	count++;
}

/*for(let x=0; x < 11; x++){
      if(x%1 === 0){
          console.log("While: " + x);
      }
  }
*/

//  Do while loop
/* Syntax :
do{
	statement;
}while (condition);
*/
// Number is similar to parseInt when converting String to Numbers
/*let number = Number(prompt("Give me a number"));

do {
	console.log("Do While: " + number);
	number +=1;
} while (number < 10);*/

// Create a new variable to be uses in displaying even number from 2-10 using while loop

let evenNumber = 2;

do {
	console.log("Even: " + evenNumber);
	evenNumber +=2;
} while (evenNumber < 12);